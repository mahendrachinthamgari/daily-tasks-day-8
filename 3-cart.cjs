const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}];


/*

Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/


// Q1. Find all the items with price more than $65.

//console.log(products[0]);

const productsPriceMoreThan65 = Object.entries(products[0]).map((item) => {
    if (Array.isArray(item[1])) {
        const materials = item[1].filter((product) => {
            let productArray = Object.entries(product).flat(1);
            return Number(productArray[1].price.replace('$', '')) > 65;
        });
        return materials;
    } else {
        return item;
    }
})
    .filter((element) => {
        if (typeof element[0] == 'object') {
            return true;
        } else {
            return Number(element[1].price.replace('$', '')) > 65;
        }
    });
//console.log(productsPriceMoreThan65);


// Q2. Find all the items where quantity ordered is more than 1.

const productsQuantityMoreThan1 = Object.entries(products[0]).map((item) => {
    if (Array.isArray(item[1])) {
        const materials = item[1].filter((product) => {
            let productArray = Object.entries(product).flat(1);
            return productArray[1].quantity > 1;
        });
        return materials;
    } else {
        return item;
    }
})
    .filter((element) => {
        if (typeof element[0] == 'object') {
            return true;
        } else {
            return element[1].quantity > 1;
        }
    });

// console.log(productsQuantityMoreThan1);


// Q.3 Get all items which are mentioned as fragile.

const fragileTypeProduct = Object.entries(products[0]).map((item) => {
    if (Array.isArray(item[1])) {
        const materials = item[1].filter((product) => {
            let productArray = Object.entries(product).flat(1);
            return productArray[1].type == 'fragile';
        });
        return materials;
    } else {
        return item;
    }
})
    .filter((element) => {
        if (typeof element[0] == 'object') {
            return true;
        } else {
            return element[1].type == 'fragile';
        }
    })

console.log(fragileTypeProduct);

//Q.4 Find the least and the most expensive item for a single quantity.
